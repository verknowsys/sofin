# Terminal definitions

LOGNAME="${LOGNAME:-${USER}}"
SHELL="${SHELL:-${ZSH_BIN}}"
TERM="${TERM:-xterm-256color}"
BLOCKSIZE="${BLOCKSIZE:-K}"

TTY="NO"
MAX_COLS=80
SUCCESS_CHAR="V"
TRY_CHAR="?"
RUN_CHAR="!"
WARN_CHAR="*"
NOTE_CHAR=">"
FAIL_CHAR="#"
NOTE_CHAR2="-"
SEPARATOR_CHAR="_"
SEPARATOR_CHAR2="-"
if [ -t 1 ]; then
    TTY="YES"
    TRY_CHAR="⁇"
    RUN_CHAR="⁈"
    SUCCESS_CHAR="✓"
    WARN_CHAR="❣"
    NOTE_CHAR="→"
    NOTE_CHAR2="»"
    FAIL_CHAR="✘"
    SEPARATOR_CHAR="┈"
    SEPARATOR_CHAR2="─"
fi

# Line replay ANSI codes:
REPLAY_CURRENT_LINE="\33[2K\r"
REPLAY_PREVIOUS_LINE="\033[A\33[2K\r"

# determine TERM capabilities:
determine_term_capabilites () {
    setopt >/dev/null 2>/dev/null # zsh only command
    if [ "${?}" = "0" ]; then
        # Zsh is still running as sh, so we want to load Zsh fancy features:
        zmodload zsh/parameter

        setopt debugbeforecmd
        setopt appendhistory
        setopt extendedhistory

        # NOTE: don't send HUP on shell exit:
        unsetopt hup

        # NOTE: required for funcstack to work:
        setopt evallineno

        # NOTE: don't do notification on signal handling:
        unsetopt monitor

        CAP_TERM_ZSH=YES
        unset CAP_TERM_BASH
    else
        # Check bash specific command (but also available in zsh):
        declare -f >/dev/null 2>/dev/null
        if [ "${?}" = "0" ]; then
            # let's assume that if shell has support for "declare -f"
            # then it also supports bashismed "arrays[*]":
            CAP_TERM_BASH=YES
            unset CAP_TERM_ZSH
        else # fallback to most limited shell:
            unset CAP_TERM_ZSH
            unset CAP_TERM_BASH
        fi
    fi

    ${TPUT_BIN} setaf 0 >/dev/null 2>/dev/null
    if [ "${?}" != "0" ]; then # broken with tput under FreeBSD, related bug: https://bugs.freebsd.org/bugzilla/show_bug.cgi?id=210858
        unset CAP_TERM_TPUT

        # Use legacy ANSI definitions:
        ColorGray="\033[39m" # default color of terminal
        ColorReset="${ColorGray}"
        ColorDarkgray="\033[90m"
        ColorRed="\033[31m"
        ColorGreen="\033[32m"
        ColorYellow="\033[33m"
        ColorOrange="\033[38;5;172m"
        ColorBlue="\033[34m"
        ColorMagenta="\033[35m"
        ColorCyan="\033[36m"
        ColorDistinct="${ColorCyan}"
        ColorWhite="\033[97m"
        ColorParams="\033[38;5;12m"
        ColorViolet="\033[38;5;97m"
        ColorDebug="${ColorViolet}"
        ColorNote="${ColorGreen}"
        ColorWarning="${ColorYellow}"
        ColorError="${ColorRed}"
        ColorExample="${ColorOrange}"
        ColorFunction="\033[38;5;163m"
    else
        CAP_TERM_TPUT=YES

        # Use tput by default for terminal color management:
        # setaf => foreground
        # setab => background
        ColorGray="$(${TPUT_BIN} setaf 7)" # default color of terminal
        ColorReset="${ColorGray}"
        ColorDarkgray="$(${TPUT_BIN} setaf 0)"
        ColorRed="$(${TPUT_BIN} setaf 1)"
        ColorGreen="$(${TPUT_BIN} setaf 2)"
        ColorYellow="$(${TPUT_BIN} setaf 3)"
        ColorOrange="$(${TPUT_BIN} setaf 172)"
        ColorBlue="$(${TPUT_BIN} setaf 4)"
        ColorMagenta="$(${TPUT_BIN} setaf 5)"
        ColorCyan="$(${TPUT_BIN} setaf 6)"
        ColorDistinct="${ColorCyan}"
        ColorWhite="$(${TPUT_BIN} setaf 15)"
        ColorParams="$(${TPUT_BIN} setaf 12)"
        ColorViolet="$(${TPUT_BIN} setaf 97)"
        ColorDebug="${ColorViolet}"
        ColorNote="${ColorGreen}"
        ColorWarning="${ColorYellow}"
        ColorError="${ColorRed}"
        ColorExample="${ColorOrange}"
        ColorFunction="$(${TPUT_BIN} setaf 163)"
    fi
}

determine_term_capabilites
