
debug () {
    _in=${*}
    if [ "${TTY}" = "YES" ]; then
        _permdbg="\n"
    else
        unset _permdbg
    fi
    if [ -n "${CAP_SYS_PRODUCTION}" ]; then
        if [ -n "${DEBUG}" ]; then
            ${PRINTF_BIN} "# (%s) λ ${ColorDebug}%s${ColorReset}\n" "${SHLVL}" "${*}"
        else
            ${PRINTF_BIN} "# (%s) λ ${ColorDebug}%s${ColorReset}\n" "${SHLVL}" "${*}" >/dev/null
        fi
    else
        _sep="${_sep:-$(distd "λ " ${ColorDarkgray})}"
        if [ "${CAP_TERM_BASH}" = "YES" ]; then
            _dbfile="$(distd "${BASH_SOURCE#/usr/share/sofin/}:${BASH_LINENO[0]}" "${ColorBlue}")"
            _fun="$(distd "${FUNCNAME[2]}()" "${ColorBlue}")"

        elif [ "${CAP_TERM_ZSH}" = "YES" ]; then
            # NOTE: $funcstack[2]; ${funcfiletrace[@]} ${funcsourcetrace[@]} ${funcstack[@]} ${functrace[@]}
            _dbfile="$(distd "${funcfiletrace[2]#/usr/share/sofin/}" "${ColorBlue}")"
            _fun="$(distd " ${funcstack[2]}()" "${ColorBlue}")"

        else
            _dbfile=""
            _fun=""
        fi

        touch_logsdir_and_logfile
        _dbfn=" (#${SHLVL}) [${_sep}${_fun} @ ${_dbfile}] "
        if [ -z "${DEBUG}" ]; then
            _dbgnme="$(lowercase "${DEF_NAME}${DEF_POSTFIX}")"
            if [ -n "${_dbgnme}" ]; then
                # Definition log
                ${PRINTF_BIN} "#${ColorDebug}%s%s${ColorReset}${_permdbg}" "${_dbfn}" "${_in}" 2>> "${LOG}-${_dbgnme}" >> "${LOG}-${_dbgnme}"
            elif [ -z "${_dbgnme}" ]; then
                # Main log
                ${PRINTF_BIN} "#${ColorDebug}%s%s${ColorReset}${_permdbg}" "${_dbfn}" "${_in}" 2>> "${LOG}" >> "${LOG}"
            elif [ ! -d "${LOGS_DIR}" ]; then
                # System logger fallback
                ${LOGGER_BIN} "# λ ${ColorDebug}${_dbfn}${_in}${ColorReset}" 2>/dev/null
            fi
        else # DEBUG is set. Print to stdout
            ${PRINTF_BIN} "#${ColorDebug}%s%s${ColorReset}\n" "${_dbfn}" "${_in}"
        fi
        unset _dbgnme _in _dbfn _dbfnin _elmz _cee
    fi
}


warn () {
    if [ "${TTY}" = "YES" ]; then
        ${PRINTF_BIN} "${REPLAY_PREVIOUS_LINE}${ColorYellow}%s${ColorReset}\n\n" "${*}"
    else
        ${PRINTF_BIN} "${ColorYellow}%s${ColorReset}\n" "${*}"
    fi
}


note () {
    if [ "${TTY}" = "YES" ]; then
        ${PRINTF_BIN} "${REPLAY_PREVIOUS_LINE}${ColorGreen}%s${ColorReset}\n" "${*}"
    else
        ${PRINTF_BIN} "${ColorGreen}%s${ColorReset}\n" "${*}"
    fi
}


permnote () {
    if [ "${TTY}" = "YES" ]; then
        ${PRINTF_BIN} "${REPLAY_PREVIOUS_LINE}${ColorGreen}%s${ColorReset}\n\n" "${*}"
    else
        ${PRINTF_BIN} "${ColorGreen}%s${ColorReset}\n" "${*}"
    fi
}


error () {
    # Get three recently modified logs:
    _last_two_mod_logs=$(${LS_BIN} -1t "${LOGS_DIR}" 2>/dev/null | ${HEAD_BIN} -3 2>/dev/null | eval "${NEWLINES_TO_SPACES_GUARD}")
    ${PRINTF_BIN} "\n\n${ColorRed}%s\n\n  ${FAIL_CHAR} Error!\n\n    %s\n\n%s\n\n${ColorReset}%s\n\n${ColorWhite}Showing tail of 3 most recent Sofin logs:\n\n${ColorReset}%s${ColorWhite}\n\n%s\n\n%s\n\n%s\n\n\n" \
        "$(fill "${SEPARATOR_CHAR2}")" "${*}" "$(fill "${SEPARATOR_CHAR2}")" "$(fill "${SEPARATOR_CHAR2}")" "$(fill "${SEPARATOR_CHAR2}")" "$(cd "${LOGS_DIR}" && ${TAIL_BIN} -n "${LOG_LINES_AMOUNT_ON_ERR}" ${_last_two_mod_logs})" "$(fill "${SEPARATOR_CHAR2}")"
    warn "  ${NOTE_CHAR2} If you think this error is a bug in definition, please report an info about"
    warn "    encountered problem on one of issue trackers:"
    ${PRINTF_BIN} "\n"
    warn "  ${NOTE_CHAR}  Github: $(distw "${DEFAULT_ISSUE_REPORT_SITE}")"
    ${PRINTF_BIN} "\n"
    warn "$(fill "${SEPARATOR_CHAR}" 46)$(distw "  Daniel (dmilith) Dettlaff  ")$(fill "${SEPARATOR_CHAR}" 5)"
    ${PRINTF_BIN} "\n"
    # TODO: add "history backtrace". Play with: fc -lnd -5, but separate sh/zsh history file should solve the problem
    finalize_interrupt
    exit "${ERRORCODE_TASK_FAILURE}"
}


# distdebug
distd () {
    ${PRINTF_BIN} "${2:-${ColorDistinct}}%s${3:-${ColorDebug}}" "${1}" 2>/dev/null
}


# distnote
distn () {
    ${PRINTF_BIN} "${2:-${ColorDistinct}}%s${3:-${ColorNote}}" "${1}" 2>/dev/null
}


# distwarn
distw () {
    ${PRINTF_BIN} "${2:-${ColorDistinct}}%s${3:-${ColorWarning}}" "${1}" 2>/dev/null
}


# disterror
diste () {
    ${PRINTF_BIN} "${2:-${ColorDistinct}}%s${3:-${ColorError}}" "${1}" 2>/dev/null
}


run () {
    _run_params=${*}
    env_forgivable
    if [ -n "${_run_params}" ]; then
        touch_logsdir_and_logfile
        ${PRINTF_BIN} '%s\n' "${_run_params}" | eval "${MATCH_PRINT_STDOUT_GUARD}" && _run_shw_prgr=YES
        # debug "$(distd "$(${DATE_BIN} ${DEFAULT_DATE_TRYRUN_OPTS} 2>/dev/null)" ${ColorDarkgray}): $(distd "${RUN_CHAR}" ${ColorWhite}): $(distd "${_run_params}" ${ColorParams}) $(distd "[show-blueout:${_run_shw_prgr:-NO}]" "${ColorBlue}")"
        if [ -z "${DEF_NAME}${DEF_POSTFIX}" ]; then
            if [ -z "${_run_shw_prgr}" ]; then
                eval "PATH=${PATH}${GIT_EXPORTS} ${_run_params}" >> "${LOG}" 2>> "${LOG}"
                check_result ${?} "${_run_params}"
            else
                ${PRINTF_BIN} "${ColorBlue}"
                eval "PATH=${PATH}${GIT_EXPORTS} ${_run_params}" >> "${LOG}"
                check_result ${?} "${_run_params}"
            fi
        else
            _rnm="$(lowercase "${DEF_NAME}${DEF_POSTFIX}")"
            if [ -z "${_run_shw_prgr}" ]; then
                eval "PATH=${PATH}${GIT_EXPORTS} ${_run_params}" >> "${LOG}-${_rnm}" 2>> "${LOG}-${_rnm}"
                check_result ${?} "${_run_params}"
            else
                ${PRINTF_BIN} "${ColorBlue}"
                eval "PATH=${PATH}${GIT_EXPORTS} ${_run_params}" >> "${LOG}-${_rnm}"
                check_result ${?} "${_run_params}"
            fi
        fi
    else
        error "Specified an empty command to run()!"
    fi
    unset _rnm _run_shw_prgr _run_params _dt
}


try () {
    _try_params=${*}
    env_forgivable
    if [ -n "${_try_params}" ]; then
        touch_logsdir_and_logfile
        ${PRINTF_BIN} "${_try_params}\n" | eval "${MATCH_PRINT_STDOUT_GUARD}" && _show_prgrss=YES
        # _dt="$(distd "$(${DATE_BIN} ${DEFAULT_DATE_TRYRUN_OPTS} 2>/dev/null)" ${ColorDarkgray})"
        # debug "${_dt}: $(distd "${TRY_CHAR}" ${ColorWhite}) $(distd "${_try_params}" ${ColorParams}) $(distd "[${_show_prgrss:-NO}]" "${ColorBlue}")"
        _try_aname="$(lowercase "${DEF_NAME}${DEF_POSTFIX}")"
        if [ -z "${_try_aname}" ]; then
            if [ -z "${_show_prgrss}" ]; then
                eval "PATH=${PATH}${GIT_EXPORTS} ${_try_params}" >> "${LOG}" 2>> "${LOG}" && \
                    env_pedantic && \
                    return 0
            else
                # show progress on stderr
                ${PRINTF_BIN} "${ColorBlue}"
                eval "PATH=${PATH}${GIT_EXPORTS} ${_try_params}" >> "${LOG}" && \
                    env_pedantic && \
                    return 0
            fi
        else
            if [ -z "${_show_prgrss}" ]; then
                eval "PATH=${PATH}${GIT_EXPORTS} ${_try_params}" >> "${LOG}-${_try_aname}" 2>> "${LOG}-${_try_aname}" && \
                    env_pedantic && \
                    return 0
            else
                ${PRINTF_BIN} "${ColorBlue}"
                eval "PATH=${PATH}${GIT_EXPORTS} ${_try_params}" >> "${LOG}-${_try_aname}" && \
                    env_pedantic && \
                    return 0
            fi
        fi
    else
        error "Specified an empty command to try()!"
    fi
    unset _dt _try_aname _try_params
    return 1
}


retry () {
    _targets=${*}
    _ammo="OOO"
    env_forgivable
    touch_logsdir_and_logfile
    # check for commands that puts something important/intersting on stdout
    ${PRINTF_BIN} '%s\n' "${_targets}" 2>/dev/null | eval "${MATCH_PRINT_STDOUT_GUARD}" && _rtry_blue=YES
    while [ -n "${_ammo}" ]; do
        if [ -n "${_targets}" ]; then
            # _dt="$(distd "$(${DATE_BIN} ${DEFAULT_DATE_TRYRUN_OPTS} 2>/dev/null)" ${ColorDarkgray})"
            # debug "${_dt}: $(distd "${TRY_CHAR}${NOTE_CHAR}${RUN_CHAR}" ${ColorWhite}) $(distd "${_targets}" ${ColorParams}) $(distd "[${_show_prgrss:-NO}]" "${ColorBlue}")"
            if [ -z "${_rtry_blue}" ]; then
                eval "PATH=${DEFAULT_PATH}${GIT_EXPORTS} ${_targets}" >> "${LOG}" 2>> "${LOG}" && \
                    unset _ammo _targets && \
                        env_pedantic && \
                        return 0
            else
                ${PRINTF_BIN} "${ColorBlue}"
                eval "PATH=${DEFAULT_PATH}${GIT_EXPORTS} ${_targets}" >> "${LOG}" && \
                    unset _ammo _targets && \
                        env_pedantic && \
                        return 0
            fi
        else
            env_pedantic
            error "Given an empty command to evaluate!"
        fi
        _ammo="$(${PRINTF_BIN} '%s\n' "${_ammo}" 2>/dev/null | ${SED_BIN} 's/O//' 2>/dev/null)"
        debug "Remaining attempts: $(distd "${_ammo}")"
    done
    debug "All available ammo exhausted to invoke a command: $(distd "${_targets}")"
    unset _ammo _targets _rtry_blue
    return 1
}



setup_defs_branch () {
    # setting up definitions repository
    if [ -z "${BRANCH}" ]; then
        BRANCH="${DEFAULT_DEFINITIONS_BRANCH}"
    fi
}


setup_defs_repo () {
    if [ -z "${REPOSITORY}" ]; then
        REPOSITORY="${DEFAULT_DEFINITIONS_REPOSITORY}"
    fi
}


cleanup_handler () {
    finalize
    debug "Normal exit."
    return 0
}


interrupt_handler () {
    warn "Interrupted: $(distw "${SOFIN_PID:-$$}")"
    finalize_interrupt
    exit "${ERRORCODE_USER_INTERRUPT}"
}


terminate_handler () {
    warn "Terminated: $(distw "${SOFIN_PID:-$$}")"
    finalize
    exit "${ERRORCODE_TERMINATED}"
}


noop_handler () {
    debug "Handled signal USR2 with noop()"
}


trap_signals () {
    trap 'cleanup_handler' EXIT
    # No    Name         Default Action       Description
    # 1     SIGHUP       terminate process    terminal line hangup
    # 2     SIGINT       terminate process    interrupt program
    # 3     SIGQUIT      create core image    quit program
    # 4     SIGILL       create core image    illegal instruction
    # 5     SIGTRAP      create core image    trace trap
    # 6     SIGABRT      create core image    abort program (formerly SIGIOT)
    # 7     SIGEMT       create core image    emulate instruction executed
    # 8     SIGFPE       create core image    floating-point exception
    # 9     SIGKILL      terminate process    kill program
    # 10    SIGBUS       create core image    bus error
    # 11    SIGSEGV      create core image    segmentation violation
    # 12    SIGSYS       create core image    non-existent system call invoked
    # 13    SIGPIPE      terminate process    write on a pipe with no reader
    # 14    SIGALRM      terminate process    real-time timer expired
    # 15    SIGTERM      terminate process    software termination signal
    # 16    SIGURG       discard signal       urgent condition present on socket
    # 17    SIGSTOP      stop process         stop (cannot be caught or ignored)
    # 18    SIGTSTP      stop process         stop signal generated from keyboard
    # 19    SIGCONT      discard signal       continue after stop
    # 20    SIGCHLD      discard signal       child status has changed
    # 21    SIGTTIN      stop process         background read attempted from control terminal
    # 22    SIGTTOU      stop process         background write attempted to control terminal
    # 23    SIGIO        discard signal       I/O is possible on a descriptor (see fcntl(2))
    # 24    SIGXCPU      terminate process    cpu time limit exceeded (see setrlimit(2))
    # 25    SIGXFSZ      terminate process    file size limit exceeded (see setrlimit(2))
    # 26    SIGVTALRM    terminate process    virtual time alarm (see setitimer(2))
    # 27    SIGPROF      terminate process    profiling timer alarm (see setitimer(2))
    # 28    SIGWINCH     discard signal       Window size change
    # 29    SIGINFO      discard signal       status request from keyboard
    # 30    SIGUSR1      terminate process    User defined signal 1
    # 31    SIGUSR2      terminate process    User defined signal 2
    #
    if [ "YES" = "${CAP_TERM_ZSH}" ]; then
        trap - ZERR
    elif [ "YES" = "${CAP_TERM_BASH}" ]; then
        trap 'error' ERR
    fi
    trap 'interrupt_handler' INT
    trap 'terminate_handler' TERM
    trap 'noop_handler' USR2 # This signal is used to "reload shell"-feature. Sofin should ignore it
    return 0
}


untrap_signals () {
    trap - EXIT
    if [ "YES" = "${CAP_TERM_ZSH}" ]; then
        trap - ZERR
    elif [ "YES" = "${CAP_TERM_BASH}" ]; then
        trap - ERR
    fi
    trap - INT
    trap - TERM
    trap - USR2
}


restore_security_state () {
    if [ "YES" = "${CAP_SYS_HARDENED}" ]; then
        if [ -f "${DEFAULT_SECURITY_STATE_FILE}" ]; then
            note "Restoring pre-build security state"
            . "${DEFAULT_SECURITY_STATE_FILE}" #>> "${LOG}" 2>> "${LOG}"
        else
            debug "No security state file found: $(distd "${DEFAULT_SECURITY_STATE_FILE}")"
        fi
    else
        debug "No hardening capabilities in system"
    fi
}


store_security_state () {
    if [ "YES" = "${CAP_SYS_HARDENED}" ]; then
        try "${RM_BIN} -f ${DEFAULT_SECURITY_STATE_FILE}"
        debug "Storing current security state to file: $(distd "${DEFAULT_SECURITY_STATE_FILE}")"
        for _key in ${DEFAULT_HARDEN_KEYS}; do
            _sss_value="$(${SYSCTL_BIN} -n "${_key}" 2>/dev/null)"
            _sss_cntnt="${SYSCTL_BIN} ${_key}=${_sss_value}"
            ${PRINTF_BIN} "${_sss_cntnt}\n" >> "${DEFAULT_SECURITY_STATE_FILE}" 2>/dev/null
        done
        unset _key _sss_cntnt _sss_value
    else
        debug "No hardening capabilities in system"
    fi
}


disable_security_features () {
    if [ "YES" = "${CAP_SYS_HARDENED}" ]; then
        debug "Disabling all security features.."
        try "${RM_BIN} -f ${DEFAULT_SECURITY_STATE_FILE}"
        for _key in ${DEFAULT_HARDEN_KEYS}; do
            try "${SYSCTL_BIN} ${_key}=0"
        done
        unset _key _dsf_name
    else
        debug "No hardening capabilities in system"
    fi
}


summary () {
    # Sofin performance counters:
    SOFIN_END="${SOFIN_END:-$(${SOFIN_MICROSECONDS_UTILITY_BIN})}"
    SOFIN_RUNTIME="$(calculate_bc "(${SOFIN_END} - ${SOFIN_START:-${SOFIN_END}}) / 1000")"

    ${PRINTF_BIN} "${ColorExample}%s${ColorReset}\n" "$(fill "${SEPARATOR_CHAR2}")" >> "${LOG:-/var/log/sofin}"
    ${PRINTF_BIN} "${ColorExample}args.head: ${ColorYellow}%s\n" "${SOFIN_COMMAND_ARG:-''}" >> "${LOG:-/var/log/sofin}"
    ${PRINTF_BIN} "${ColorExample}args.tail: ${ColorYellow}%s\n" "${SOFIN_ARGS:-''}" >> "${LOG:-/var/log/sofin}"
    ${PRINTF_BIN} "${ColorExample}pid: ${ColorYellow}%d${ColorExample}\n" "${SOFIN_PID:--1}" >> "${LOG:-/var/log/sofin}"
    ${PRINTF_BIN} "${ColorExample}runtime: ${ColorYellow}%d${ColorExample} ms.\n" "${SOFIN_RUNTIME:--1}" >> "${LOG:-/var/log/sofin}"

    # Show "times" stats from shell:
    ${PRINTF_BIN} "${ColorExample}shell times: ${ColorReset}\n" >> "${LOG:-/var/log/sofin}"
    times >> "${LOG:-/var/log/sofin}"

    ${PRINTF_BIN} "${ColorExample}%s${ColorReset}\n" "$(fill "${SEPARATOR_CHAR2}")" >> "${LOG:-/var/log/sofin}"
}


initialize () {
    setup_defs_branch
    setup_defs_repo
    check_defs_dir
    check_os
    trap_signals

    if [ "${TTY}" = "YES" ]; then
        # turn echo off
        ${STTY_BIN} -echo
    fi
}
